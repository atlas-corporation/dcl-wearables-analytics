import { QueryResult } from "@apollo/client";

export interface DataContextSchema {
  collections: QueryResult;
  counts: QueryResult;
  getCreator: Function;
  creators: Creators;
  collectionsLoaded: boolean;
}

export type Creators = { [key: string]: Creator };

export interface CollectionData {
  id: string;
  creator: string;
  numCollections?: number;
  itemsCount?: number;
  createdAt: string;
}

export interface Collection {
  id: string;
  creator: string;
  name: string;
  owner: string;
  createdAt: string;
  itemsCount: number;
  items: Item[];
}

export interface CreatorTotalData<> {
  creator: string;
  total: number;
}

export type Creator = {
  avatars: {
    userId: string;
    email: string;
    name: string;
    hasClaimedName: boolean;
    description: string;
    ethAddress: string;
    version: number;
    avatar: any;
    tutorialStep: number;
    interests: any[];
    unclaimedName: string;
  }[];
  sales?: Sale[];
};

export type Item = { id: string; image: string; rarity?: Rarity };

export type Rarity =
  | "common"
  | "uncommon"
  | "rare"
  | "epic"
  | "legendary"
  | "mythic"
  | "unique";
export interface Sale {
  id: string;
  type: string;
  buyer: string;
  seller: string;
  price: number;
  item: {
    id: string;
    metadata: {
      wearable: {
        name: string;
      };
    };
    collection: {
      id: string;
      name: string;
    };
  };
  timestamp: string;
  txHash: string;
  searchItemId: number;
  searchTokenId: number;
}
