export type Width =
  | "1/2"
  | "1"
  | "1/4"
  | "1/5"
  | "1/3"
  | "2/3"
  | "3/4"
  | "custom";

export type Direction = "row" | "col";

export type Size = "sm" | "md" | "lg" | "xl";

export type Border = boolean;

export type Alignment = "center" | "end" | "start" | "baseline" | "stretch";

export type Justify =
  | "center"
  | "between"
  | "around"
  | "evenly"
  | "end"
  | "start";

export type Position = "static" | "fixed" | "absolute" | "relative" | "sticky";
