const decentralandGraphUrl =
  "https://api.thegraph.com/subgraphs/name/decentraland/collections-matic-mainnet";

const decentralandPeerUrl = "https://peer.decentraland.org";

export { decentralandGraphUrl, decentralandPeerUrl };
