import React from "react";

const ManaSVG: React.FC = () => {
  return <img src={data} alt="mana_symbol" />;
};

export default ManaSVG;

const data = `data:image/svg+xml;charset=utf-8,%3Csvg width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M12 0l12 12-12 12L0 12 12 0zm0 3.36L20.64 12 12 20.64 3.36 12 12 3.36zm0 12.96a4.32 4.32 0 10.001-8.64 4.32 4.32 0 000 8.64z' fill='%2316141A'/%3E%3C/svg%3E`;
