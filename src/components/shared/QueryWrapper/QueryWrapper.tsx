import React from "react";

interface QueryWrapperProps {
  loading: boolean;
  children: React.ReactChild | React.ReactChild[] | null;
}

// Provides global error/loading handling for components that request data from GraphQl API using apollo
const QueryWrapper: React.FC<QueryWrapperProps> = (props) => {
  const { loading, children } = props;

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!loading) {
    return <div>{children}</div>;
  }

  return null;
};

export default QueryWrapper;
