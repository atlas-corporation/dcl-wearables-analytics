module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        site: {
          primary: "#FF3F55",
        },
        rarity: {
          "common": "rgb(171, 193, 193)",
          "uncommon": "rgb(237, 109, 79)",
          "rare": "rgb(54, 207, 117)",
          "epic": "rgb(61, 133, 230)",
          "legendary": "rgb(132, 45, 218)",
          "mythic": "rgb(255, 99, 225)",
          "unique": "rgb(255, 182, 38)",
        },
      },
    },
  },
  plugins: [],
}
